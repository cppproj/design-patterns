//============================================================================
// Name        : Mediator.h
// Created on  : 17.08.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Mediator pattern tests
//============================================================================

#ifndef MEDIATOR_PATTERN__TESTS_H__
#define MEDIATOR_PATTERN__TESTS_H__

#include <iostream>
#include <string>
#include <memory>

namespace Mediator {

	/**
	 * The Mediator interface declares a method used by components to notify the
	 * mediator about various events. The Mediator may react to these events and
	 * pass the execution to other components.
	 */
	class Mediator {
	public:
		virtual void Notify(std::shared_ptr<class BaseComponent> sender,
			                const std::string& event) const noexcept = 0;
	};


	/**
	 * The Base Component provides the basic functionality of storing a mediator's
	 * instance inside component objects.
	 */
	class BaseComponent {
	protected:
		std::shared_ptr<Mediator> mediator;

	public:
		BaseComponent(std::shared_ptr<Mediator> mediator = nullptr);
		void setMediator(std::shared_ptr<Mediator> mediator);
	};


	/**
	 * Concrete Components implement various functionality. They don't depend on
	 * other components. They also don't depend on any concrete mediator classes.
	 */
	class Component1 : public BaseComponent, 
					   public std::enable_shared_from_this<Component1> {
	public:
		void DoA();
		void DoB();
	};

	class Component2 : public BaseComponent,
					   public std::enable_shared_from_this<Component2> {
	public:
		void DoC();
		void DoD();
	};


	/**
	 * Concrete Mediators implement cooperative behavior by coordinating several
	 * components.
 */
	class ConcreteMediator: public Mediator,
							public std::enable_shared_from_this<ConcreteMediator> {
	private:
		std::shared_ptr<Component1> component1;
		std::shared_ptr<Component2> component2;

	public:
		ConcreteMediator(std::shared_ptr<Component1> c1, std::shared_ptr<Component2> c2);
		void Notify(std::shared_ptr<BaseComponent> sender,
				    const std::string& event) const noexcept override;

		void Init();
	};


	void Test();
}

#endif // !(MEDIATOR_PATTERN__TESTS_H__)