//============================================================================
// Name        : Mediator.cpp
// Created on  : 17.08.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Mediator pattern tests
//============================================================================

#include "Mediator.h"
#include <cassert>

namespace Mediator {

	/**
	 * The Base Component provides the basic functionality of storing a mediator's
	 * instance inside component objects.
	 */
	BaseComponent::BaseComponent(std::shared_ptr<Mediator> mediator): mediator(mediator) {
	}

	void BaseComponent::setMediator(std::shared_ptr<Mediator> mediator) {
		this->mediator = mediator;
	}

	/**
	 * Concrete Components implement various functionality. They don't depend on
	 * other components. They also don't depend on any concrete mediator classes.
	 */
	void Component1::DoA() {
		std::cout << "Component 1 does A" << std::endl;
		this->mediator->Notify(this->shared_from_this(), "A");
	}
		
	void Component1::DoB() {
		std::cout << "Component 1 does B" << std::endl;
		this->mediator->Notify(this->shared_from_this(), "B");
	}


	void Component2::DoC() {
		std::cout << "Component 2 does C" << std::endl;
		this->mediator->Notify(this->shared_from_this(), "C");
	}

	void Component2::DoD() {
		std::cout << "Component 2 does D" << std::endl;
		this->mediator->Notify(this->shared_from_this(), "D");
	}

	/**
	 * Concrete Mediators implement cooperative behavior by coordinating several
	 * components.
	 */
	ConcreteMediator::ConcreteMediator(std::shared_ptr<Component1> c1,
									   std::shared_ptr<Component2> c2): component1(c1), component2(c2)
	{
	}

	void ConcreteMediator::Notify(std::shared_ptr<BaseComponent> sender,
								  const std::string& event) const noexcept  {
		if (event == "A") {
			std::cout << "Mediator reacts on A and triggers following operations:\n";
			this->component2->DoC();
		}
		if (event == "D") {
			std::cout << "Mediator reacts on D and triggers following operations:\n";
			this->component1->DoB();
			this->component2->DoC();
		}
	}

	void ConcreteMediator::Init()
	{
		assert(nullptr != this->shared_from_this());
		this->component1->setMediator(this->shared_from_this());
		this->component2->setMediator(this->shared_from_this());
	}
}

void Mediator::Test()
{
	std::shared_ptr<Component1> c1 = std::make_shared<Component1>();
	std::shared_ptr<Component2> c2 = std::make_shared<Component2>();

	std::shared_ptr<ConcreteMediator> mediator = std::make_shared<ConcreteMediator>(c1, c2);
	mediator->Init();

	std::cout << "Client triggers operation A." << std::endl;
	c1->DoA();
	std::cout << "\nClient triggers operation D." << std::endl;
	c2->DoD();
}

