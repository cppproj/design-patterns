//============================================================================
// Name        : ObserverEx.cpp
// Created on  : September 18, 2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : ObserverEx pattern test
//============================================================================

#include "ObserverEx.h"
#include <algorithm>

namespace Observer_Tests_Ex {

	/*
	template<class T>
	class IObserver {
	public:
		virtual void Update(const T&) = 0;
		virtual ~IObserver() = default;
	};

	template<class T>
	class IObservable {
	public:
		virtual void addObserver(std::shared_ptr<T> observer) = 0;
		virtual void deleteObserver(const std::shared_ptr<T>& observer) = 0;
		
		virtual ~IObservable() = default;
	};

	template<class T>
	class TestObserver : public IObserver<T> {
		virtual void Update(const T& params ) override {
			std::cout << params << std::endl;
		}
	};

	template<class T = std::string>
	class Worker : public IObservable<T> {
	protected:
		std::vector<std::shared_ptr<TestObserver<T>>> observers_;

	public:
		virtual void addObserver(std::shared_ptr<TestObserver<T>> observer) override {
			this->observers_.push_back(observer);
		}
	};

	*/

	class IObserver {
	public:
		virtual void Update(const std::string& params) = 0;
		virtual ~IObserver() = default;
	};

	class IObservable {
	public:
		virtual void addObserver(std::shared_ptr<IObserver> observer) = 0;
		virtual void deleteObserver(const std::shared_ptr<IObserver>& observer) = 0;
		virtual ~IObservable() = default;
	};


	class TestObserver : public IObserver {
		virtual void Update(const std::string& params) override {
			std::cout << params << std::endl;
		}
	};

	class Worker : public IObservable {
	protected:
		std::vector<std::shared_ptr<IObserver>> observers_;

	public:
		virtual void addObserver(std::shared_ptr<IObserver> observer) override {
			this->observers_.push_back(observer);
		}

		/*
		template<typename ... T>
		decltype(auto) emplacedObserver(T&& ... params) {
			return this->observers_.emplace_back(std::forward<T>(params)...);
		}
		*/

		virtual void deleteObserver(const std::shared_ptr<IObserver>& observer) override {
			std::cout << "size = " << observers_.size() << std::endl;
			auto iter = remove_if(observers_.begin(), observers_.end(), [&observer](const auto& ptr) {
				return (ptr.get() == observer.get());
			});
			if (observers_.end() != iter)
				observers_.erase(iter, observers_.end());
			std::cout << "size = " << observers_.size() << std::endl;
		}

		void NotifyObservers(const std::string& str) {
			for (auto& observer : this->observers_)
				observer->Update(str);
		}

		void SendMessage(const std::string& str) {
			NotifyObservers(str);
		}
	};
}

void Observer_Tests_Ex::Test() {
	Worker worker;

	std::shared_ptr<IObserver> observer1 = std::make_shared<TestObserver>();
	worker.addObserver(observer1);

	std::shared_ptr<IObserver> observer2 = std::make_shared<TestObserver>();
	worker.addObserver(observer2);

	std::shared_ptr<IObserver> observer3 = std::make_shared<TestObserver>();
	worker.addObserver(observer3);


	// std::shared_ptr<IObserver> observer2 = worker.emplacedObserver();
	// std::shared_ptr<IObserver> observer3 = worker.emplacedObserver();

	worker.SendMessage("111");

	worker.deleteObserver(observer2);
	std::cout << std::endl;

	worker.SendMessage("222");
}