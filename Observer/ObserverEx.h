//============================================================================
// Name        : ObserverEx.h
// Created on  : September 18, 2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : ObserverEx pattern test
//============================================================================

#include <iostream>
#include <string>
#include <vector>

#ifndef OBSERVER_EX_PATTERN_INCLUDE_GUARD__H
#define OBSERVER_EX_PATTERN_INCLUDE_GUARD__H

namespace Observer_Tests_Ex {

	void Test();
}

#endif // !#endif // !OBSERVER_PATTERN_INCLUDE_GUARD__H