//============================================================================
// Name        : Memento.cpp
// Created on  : August 14, 2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Memento pattern test
//============================================================================

#include "Memento.h"

namespace Memento {

	ConcreteMemento::ConcreteMemento(const std::string & state) : state(state) {
		std::time_t now = std::time(0);
		this->date = std::ctime(&now);
	}


	/**
	 * The Originator holds some important state that may change over time. It also
	 * defines a method for saving the state inside a memento and another method for
	 * restoring the state from it.
	 */
	Originator::Originator(const std::string& state) : state(state) {
		std::cout << "Originator: My initial state is: " << this->state << std::endl;
	}

	std::string Originator::GenerateRandomString(size_t length) const noexcept {
		char alphanum[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		constexpr size_t stringLength = sizeof(alphanum) - 1;
		static_assert(stringLength > 1);

		std::string random_string;
		for (size_t i = 0; i < length; i++) {
			random_string += alphanum[std::rand() % stringLength];
		}
		return random_string;
	}

	/**
	 * The Originator's business logic may affect its internal state. Therefore,
	 * the client should backup the state before launching methods of the business
	 * logic via the save() method.
	 */
	void Originator::DoSomething() {
		std::cout << "Originator: I'm doing something important." << std::endl;
		this->state = this->GenerateRandomString(30);
		std::cout << "Originator: and my state has changed to: " << this->state << std::endl;
	}

	/**
	 * Saves the current state inside a memento.
	 */
	std::shared_ptr<Memento> Originator::Save() {
		return std::make_shared<ConcreteMemento>(this->state);
	}

	/**
	 * Restores the Originator's state from a memento object.
	 */
	void Originator::Restore(std::shared_ptr<Memento> memento) {
		this->state = memento->getState();
		std::cout << "Originator: My state has changed to: " << this->state << std::endl;
	}

	/**
	 * The Caretaker doesn't depend on the Concrete Memento class. Therefore, it
	 * doesn't have access to the originator's state, stored inside the memento. It
	 * works with all mementos via the base Memento interface.
	 */
	Caretaker::Caretaker(std::shared_ptr<Originator> originator) : originator(originator) {
	
	}

	void Caretaker::Backup() {
		std::cout << "\nCaretaker: Saving Originator's state..." << std::endl;
		this->backUps.push_back(this->originator->Save());
	}

	void Caretaker::Undo() {
		if (true == this->backUps.empty()) 
			return;

		std::shared_ptr<Memento> memento = this->backUps.back();
		this->backUps.pop_back();
		std::cout << "Caretaker: Restoring state to: " << memento->getName() << std::endl;

		try {
			this->originator->Restore(memento);
		} catch (...) {
			this->Undo();
		}
	}

	void Caretaker::ShowHistory() const {
		std::cout << "History:" << std::endl;
		for (const auto memento : this->backUps) {
			std::cout << "  " << memento->getName() << std::endl;
		}
	}

	// Driver code
	void Driver() {
		std::srand(static_cast<unsigned int>(std::time(nullptr)));

		std::shared_ptr<Originator> originator = std::make_shared<Originator>("Super-duper-super-puper-super.");
		std::shared_ptr<Caretaker> caretaker = std::make_shared<Caretaker>(originator);
		caretaker->Backup();
		originator->DoSomething();
		caretaker->Backup();
		originator->DoSomething();
		caretaker->Backup();
		originator->DoSomething();
		std::cout << "\n";
		caretaker->ShowHistory();
		std::cout << "\nClient: Now, let's rollback!\n\n";
		caretaker->Undo();
		std::cout << "\nClient: Once more!\n\n";
		caretaker->Undo();
	}
}