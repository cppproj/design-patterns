//============================================================================
// Name        : Memento.h
// Created on  : August 14, 2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Memento pattern test
//============================================================================

#ifndef MEMENTO_PATTERN_INCLUDE_GUARD__H
#define MEMENTO_PATTERN_INCLUDE_GUARD__H

#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <string>
#include <memory>
#include <chrono>
#include <iomanip>
#include <ctime>
#include <vector>

namespace Memento {

	/**
	 * The Memento interface provides a way to retrieve the memento's metadata, such
	 * as creation date or name. However, it doesn't expose the Originator's state.
	 */
	class Memento {
	public:
		virtual std::string getName()  const noexcept = 0;
		virtual std::string getDate()  const noexcept = 0;
		virtual std::string getState() const noexcept = 0;
	};


	/**
	 * The Concrete Memento contains the infrastructure for storing the Originator's
	 * state.
	 */
	class ConcreteMemento : public Memento {
	private:
		std::string state;
		std::string date;

	public:
		ConcreteMemento(const std::string& state);

		/**
		 * The Originator uses this method when restoring its state.
		 */
		inline std::string getState() const noexcept override {
			return this->state;
		}

		/**
		 * The rest of the methods are used by the Caretaker to display metadata.
		 */
		// TODO: I dont sure about using 'inline' here
		inline std::string getName() const noexcept override {
			return this->date + " / (" + this->state.substr(0, 9) + "...)";
		}

		/**
		 * Returns the Date as string.
		 */
		inline std::string getDate() const noexcept override {
			return this->date;
		}
	};

	/**
	 * The Originator holds some important state that may change over time. It also
	 * defines a method for saving the state inside a memento and another method for
	 * restoring the state from it.
	 */
	class Originator {
	private:
		std::string state;

	protected:
		std::string GenerateRandomString(size_t length = 10) const noexcept;

	public:
		Originator(const std::string& state);

		/**
		 * The Originator's business logic may affect its internal state. Therefore,
		 * the client should backup the state before launching methods of the business
		 * logic via the save() method.
		 */
		void DoSomething();

		/**
		 * Saves the current state inside a memento.
		 */
		std::shared_ptr<Memento> Save();

		/**
		 * Restores the Originator's state from a memento object.
		 */
		void Restore(std::shared_ptr<Memento> memento);
	};


	/**
	 * The Caretaker doesn't depend on the Concrete Memento class. Therefore, it
	 * doesn't have access to the originator's state, stored inside the memento. It
	 * works with all mementos via the base Memento interface.
	 */
	class Caretaker {
	private:
		// Vector to store saved states:
		std::vector<std::shared_ptr<Memento>> backUps;

		//  Originator
		std::shared_ptr<Originator> originator;

	public:
		Caretaker(std::shared_ptr<Originator> originator);

		void Backup();
		void Undo();
		void ShowHistory() const;
	};




	void Driver();
};

#endif // !MEMENTO_PATTERN_INCLUDE_GUARD__H
