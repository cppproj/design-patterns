//============================================================================
// Name        : Prototype.cpp
// Created on  : October 26, 2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Prototype pattern test
//============================================================================

#include "Prototype.h"

#include <iostream>
#include <string>
#include <memory>
#include <unordered_map>

namespace Prototype {

	enum class Type: unsigned int {
		PROTOTYPE_1 = 0,
		PROTOTYPE_2
	};

	struct PrototypeTypeHasher {
		std::size_t operator()(Type type) const noexcept {
			return std::hash<int>{}( static_cast<int>(type));
		}
	};

	class Prototype {
	protected:
		std::string prototype_name_;
		int prototype_field_;

	public:
		Prototype() = default;
		Prototype(std::string prototype_name): prototype_name_(std::move(prototype_name)) {
		}

		virtual ~Prototype() = default;

		virtual Prototype *Clone() const noexcept = 0;

		virtual void Method(int prototype_field) {
			this->prototype_field_ = prototype_field;
			std::cout << "Call Method from " << prototype_name_ << " with field : " << prototype_field << std::endl;
		}

		friend struct PrototypeHash;
	};

	struct PrototypeHash {
		std::size_t operator()(const Prototype& s) const noexcept {
			return std::hash<int>{}(s.prototype_field_) +
				std::hash<std::string>{}(s.prototype_name_);
		}
	};

	class ConcretePrototype1 : public Prototype {
	private:
		int concrete_prototype_field;

	public:
		ConcretePrototype1(std::string prototype_name, int concrete_prototype_field)
			: Prototype(prototype_name), concrete_prototype_field(concrete_prototype_field) {
		}

		/**
		 * Notice that Clone method return a Pointer to a new ConcretePrototype1
		 * replica. so, the client (who call the clone method) has the responsability
		 * to free that memory. I you have smart pointer knowledge you may prefer to
		 * use unique_pointer here.
		 */
		Prototype *Clone() const noexcept override {
			return new ConcretePrototype1(*this);
		}
	};

	class ConcretePrototype2 : public Prototype {
	private:
		int concrete_prototype_field;

	public:
		ConcretePrototype2(std::string prototype_name, int concrete_prototype_field)
			: Prototype(prototype_name), concrete_prototype_field(concrete_prototype_field) {
		}

		Prototype *Clone() const noexcept override {
			return new ConcretePrototype2(*this);
		}
	};


	/**
	 * In PrototypeFactory you have two concrete prototypes, one for each concrete
	 * prototype class, so each time you want to create a bullet , you can use the
	 * existing ones and clone those.
	 */
	class PrototypeFactory {
	private:
		std::unordered_map<Type, Prototype*, PrototypeTypeHasher> prototypes_;

	public:
		PrototypeFactory() {
			prototypes_[Type::PROTOTYPE_1] = new ConcretePrototype1("PROTOTYPE_1 ", 50);
			prototypes_[Type::PROTOTYPE_2] = new ConcretePrototype2("PROTOTYPE_2 ", 60);
		}

		/**
		 * Be carefull of free all memory allocated. Again, if you have smart pointers
		 * knowelege will be better to use it here.
		 */
		~PrototypeFactory() {
			delete prototypes_[Type::PROTOTYPE_1];
			delete prototypes_[Type::PROTOTYPE_2];
		}

		/**
		 * Notice here that you just need to specify the type of the prototype you
		 * want and the method will create from the object with this type.
		 */
		Prototype *CreatePrototype(Type type) {
			return prototypes_[type]->Clone();
		}
	};

	void Client(PrototypeFactory* prototype_factory) {
		std::cout << "Let's create a Prototype 1\n";
		
		Prototype *prototype = prototype_factory->CreatePrototype(Type::PROTOTYPE_1);
		
		prototype->Method(90);
		delete prototype;

		std::cout << "\n";

		std::cout << "Let's create a Prototype 2 \n";

		prototype = prototype_factory->CreatePrototype(Type::PROTOTYPE_2);
		prototype->Method(10);

		delete prototype;
	}

	void Test1() {
		std::unique_ptr<PrototypeFactory> prototype_factory = std::make_unique<PrototypeFactory>();
		Client(prototype_factory.get());
	}
};

void Prototype::Test() {

	Test1();
}