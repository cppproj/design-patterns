//============================================================================
// Name        : Composite.h
// Created on  : August 25, 2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Composite design pattern demo
//============================================================================

#ifndef COMPOSITE_DESIGN_PATTERN_TESTS__H_
#define COMPOSITE_DESIGN_PATTERN_TESTS__H_

#include <iostream>
#include <string>
#include <memory>
#include <vector>

namespace Composite::Pages {

	class IPage {
	public:
		virtual void Add(IPage page) noexcept {}
		virtual void Remove() noexcept {}
		virtual void Delete(IPage page) noexcept {}
	};

	class Page : public IPage {
	public:
		void Add(IPage page) noexcept override;
		void Remove() noexcept override; 
		void Delete(IPage page) noexcept override;
	};


	class Copy : public IPage {
		std::vector<IPage> copyPages;

	public:
		void AddElement(IPage page);
		void Add(IPage page) noexcept override;
		void Remove() noexcept override;
		void Delete(IPage page)  noexcept override;
	};

	void Test();
};


namespace Composite::Graphis {

	class IGraphic {
	public:
		virtual void print() const noexcept = 0;
	};

	class Ellipse: public IGraphic {
	public:
		virtual void print() const noexcept override;
	};

	class CompositeGraphic : public IGraphic {
	private:
		std::vector<std::shared_ptr<IGraphic>>  graphicList;

	public:
		void print() const noexcept;
		void add(std::shared_ptr<IGraphic> aGraphic);
	};
}

namespace Composite {
	void Test();
};

#endif // !COMPOSITE_DESIGN_PATTERN_TESTS__H_