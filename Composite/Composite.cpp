//============================================================================
// Name        : Composite.cpp
// Created on  : August 25, 2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Composite design pattern demo
//============================================================================

#include "Composite.h"

namespace Composite::Pages
{
	void Page::Add(IPage page) noexcept {
		std::cout << "something is added to the page" << std::endl;
	}

	void Page::Remove() noexcept {
		std::cout << "soemthing is removed from the page" << std::endl;
	}

	void Page::Delete(IPage page) noexcept {
		std::cout << "soemthing is deleted from page " << std::endl;
	}



	void Copy::AddElement(IPage page) {
		copyPages.push_back(page);
	}

	void Copy::Add(IPage page) noexcept {
		std::cout << "something is added to the copy" << std::endl;
	}

	void Copy::Remove() noexcept {
		std::cout << "something is removed from the copy" << std::endl;
	}

	void Copy::Delete(IPage page) noexcept {
		std::cout << "something is deleted from the copy";
	}

	void Test() {
		Page a;
		Page b;
		Copy allcopy;
		allcopy.AddElement(a);
		allcopy.AddElement(b);

		allcopy.Add(a);
		a.Add(b);

		allcopy.Remove();
		b.Remove();
	}
}


namespace Composite::Graphis {

	void Ellipse::print() const noexcept {
		std::cout << "Ellipse" << std::endl;
	}

	void CompositeGraphic::print() const noexcept {
		for (const auto& a : this->graphicList) {
			a->print();
		}
	}

	void CompositeGraphic::add(std::shared_ptr<IGraphic> aGraphic) {
		this->graphicList.push_back(aGraphic);
	}


	void Test() {

		auto ellipse1 = std::make_shared<Ellipse>();
		auto ellipse2 = std::make_shared<Ellipse>();
		auto ellipse3 = std::make_shared<Ellipse>();
		auto ellipse4 = std::make_shared<Ellipse>();

		// Initialize three composite graphics
		auto graphic = std::make_shared<CompositeGraphic>();
		auto graphic1 = std::make_shared<CompositeGraphic>();
		auto graphic2 = std::make_shared<CompositeGraphic>();

		// Composes the graphics
		graphic1->add(ellipse1);
		graphic1->add(ellipse2);
		graphic1->add(ellipse3);

		graphic2->add(ellipse4);

		graphic->add(graphic1);
		graphic->add(graphic2);

		// Prints the complete graphic (four times the string "Ellipse")
		graphic->print();

	}
}

void Composite::Test()
{
	// Pages::Test();

	Graphis::Test();
}
