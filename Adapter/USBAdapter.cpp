//============================================================================
// Name        : USBAdapter.cpp
// Created on  : 15.08.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Adapter tests
//============================================================================

#include "USBAdapter.h"

namespace Adapter::USB_Adatpter {

	void MemoryCard::insert() {
		std::cout << "Memory card inserted successfully!" << std::endl;
	}

	void MemoryCard::copyData() {
		std::cout << "Data has been copied to your computer!" << std::endl;
	}


	CardReader::CardReader(std::unique_ptr<MemoryCard> memoryCard) : card(std::move(memoryCard)) {
		// Do smth
	}

	void CardReader::connectWithUsbCable() noexcept {
		this->card->insert();
		this->card->copyData();
	}
}

void Adapter::USB_Adatpter::USB_Adapter_Test()
{
	CardReader reader(std::make_unique<MemoryCard>());
	reader.connectWithUsbCable();
}
