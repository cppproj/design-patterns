#//============================================================================
// Name        : USBAdapter.h
// Created on  : 15.08.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Adapter tests
//============================================================================

#ifndef USB_ADAPTER_TESTS__H_
#define USB_ADAPTER_TESTS__H_

#include <iostream>
#include <memory>

namespace Adapter::USB_Adatpter {

	class USBDevice {
	public:
		virtual void connectWithUsbCable() noexcept = 0;
	};

	class MemoryCard {
	public:
		void insert();
		void copyData(); 
	};


	class CardReader : public USBDevice {
	private:
		std::unique_ptr<MemoryCard> card;

	public:
		CardReader(std::unique_ptr<MemoryCard> memoryCard);
		virtual void connectWithUsbCable() noexcept override;
	};


	void USB_Adapter_Test();
}

#endif /* USB_ADAPTER_TESTS__H_ */

