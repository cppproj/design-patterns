//============================================================================
// Name        : Filter.cpp
// Created on  : 13.08.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Filter pattern tests
//============================================================================

#include "Filter.h"

namespace Filter {

	std::ostream& operator<<(std::ostream& stream, const Person& person) {
		stream << "Name: " << person.name << ", Gender: " << person.gender << ", Status: " << person.maritalStatus;
		return stream;
	}

	std::vector<Person> CriteriaMale::meetCriteria(const std::vector<Person>& persons) {
		std::vector<Person> result;
		result.reserve(persons.size());
		for (const auto& P : persons)
			if (0 == P.getGender().compare("Male"))
				result.push_back(P);
		result.shrink_to_fit();
		// The copy elision should cause the return value optimization. I hope
		return result;
	}

	std::vector<Person> CriteriaFemale::meetCriteria(const std::vector<Person>& persons) {
		std::vector<Person> result;
		result.reserve(persons.size());
		for (const auto& P : persons)
			if (0 == P.getGender().compare("Female"))
				result.push_back(P);
		result.shrink_to_fit();
		return result;
	}

	std::vector<Person> CriteriaSingle::meetCriteria(const std::vector<Person>& persons) {
		std::vector<Person> result;
		result.reserve(persons.size());
		for (const auto& P : persons)
			if (0 == P.getMaritalStatus().compare("Single"))
				result.push_back(P);
		result.shrink_to_fit();
		return result;
	}


	AND_Criteria::AND_Criteria(std::shared_ptr<Criteria> first,
		                       std::shared_ptr<Criteria> second): criteriaFirst(first), criteriaSecond(second) {
	}

	std::vector<Person> AND_Criteria::meetCriteria(const std::vector<Person>& persons)
	{
		std::vector<Person> result = this->criteriaFirst->meetCriteria(persons);
		return criteriaSecond->meetCriteria(result);
	}


	OR_Criteria::OR_Criteria(std::shared_ptr<Criteria> first,
					         std::shared_ptr<Criteria> second) : criteriaFirst(first), criteriaSecond(second) {
	}

	std::vector<Person> OR_Criteria::meetCriteria(const std::vector<Person>& persons)
	{
		std::vector<Person> items1 = this->criteriaFirst->meetCriteria(persons);
		std::vector<Person> items2 = this->criteriaSecond->meetCriteria(persons);
		// TODO: Fix
		throw std::runtime_error("Method unimplemented");
		return items1;
	}

	// Print util:
	void printPersons(const std::string& text, const std::vector<Person>& persons) {
		std::cout << text << std::endl;
		for (const auto& P : persons)
			std::cout << P << std::endl;
	}

}


void Filter::Test()
{
	std::vector<Person> persons;

	persons.emplace_back("Robert", "Male", "Single");
	persons.emplace_back("John", "Male", "Married");
	persons.emplace_back("Laura", "Female", "Married");
	persons.emplace_back("Diana", "Female", "Single");
	persons.emplace_back("Mike", "Male", "Single");
	persons.emplace_back("Bobby", "Male", "Single");

	std::shared_ptr<Criteria> male = std::make_shared<CriteriaMale>();
	std::shared_ptr<Criteria> female = std::make_shared<CriteriaFemale>();
	std::shared_ptr<Criteria> single = std::make_shared<CriteriaSingle>();
	std::shared_ptr<Criteria> singleMale = std::make_shared<AND_Criteria>(single, male);
	std::shared_ptr<Criteria> singleOrFemale = std::make_shared<OR_Criteria>(single, female);

	
	printPersons("Males: ", male->meetCriteria(persons));
	printPersons("\nFemales: ", female->meetCriteria(persons));
	printPersons("\nSingle: ", singleMale->meetCriteria(persons));

	printPersons("\nSingle Males: ", singleMale->meetCriteria(persons));
	// printPersons("\nSingle or FeMales: ", singleOrFemale->meetCriteria(persons));
}
