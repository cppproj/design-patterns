//============================================================================
// Name        : Filter.h
// Created on  : 13.08.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Filter pattern tests
//============================================================================

#ifndef FILTER_DESIGN_PATTERN_TESTS__H_
#define FILTER_DESIGN_PATTERN_TESTS__H_

#include <iostream>
#include <string>
#include <memory>

#include <vector>

namespace Filter {

	/** Person class: **/
	class Person {
	private:
		std::string name;
		std::string gender;
		std::string maritalStatus;

	public:
		Person(const std::string& name,
			    const std::string& gender,
			    const std::string& maritalStatus): name(name), gender(gender), maritalStatus(maritalStatus) {
		}

		inline const std::string& getName() const noexcept {
			return name;
		}

		inline const std::string& getGender() const noexcept {
			return gender;
		}

		inline const std::string& getMaritalStatus() const noexcept  {
			return maritalStatus;
		}

		friend std::ostream& operator<<(std::ostream& stream, const Person& person);
	};

	/** Criteria interface: **/
	class Criteria {
	public:
		virtual std::vector<Person> meetCriteria(const std::vector<Person>& persons) = 0;
	};

	/** CriteriaMale. **/
	class CriteriaMale: public Criteria {
	public:
		virtual std::vector<Person> meetCriteria(const std::vector<Person>& persons) override;
	};

	/** CriteriaFemale. **/
	class CriteriaFemale: public Criteria {
	public:
		virtual std::vector<Person> meetCriteria(const std::vector<Person>& persons) override;
	};

	/** CriteriaSingle. **/
	class CriteriaSingle: public Criteria {
	public:
		virtual std::vector<Person> meetCriteria(const std::vector<Person>& persons) override;
	};

	/** AND_Criteria. **/
	class AND_Criteria: public Criteria {
	protected:
		std::shared_ptr<Criteria> criteriaFirst;  
		std::shared_ptr<Criteria> criteriaSecond;

	public:
		AND_Criteria(std::shared_ptr<Criteria> first, std::shared_ptr<Criteria> second);
		
	public:
		virtual std::vector<Person> meetCriteria(const std::vector<Person>& persons) override;
	};


	/** OR_Criteria. **/
	class OR_Criteria : public Criteria {
	protected:
		std::shared_ptr<Criteria> criteriaFirst;
		std::shared_ptr<Criteria> criteriaSecond;

	public:
		OR_Criteria(std::shared_ptr<Criteria> first, std::shared_ptr<Criteria> second);

	public:
		virtual std::vector<Person> meetCriteria(const std::vector<Person>& persons) override;
	};

};


namespace Filter {

	void Test();
};

#endif