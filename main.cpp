//============================================================================
// Name        : mainn.cpp
// Created on  : 30.04.2020
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Software design patterns C++ test project
//============================================================================

#include <chrono>
#include <thread>


#include "Prototype/Prototype.h"
#include "Interpreter/Interpreter.h"
#include "Composite/Composite.h"
#include "AbstractFactory/AbstractFactory.h"
#include "Adapter/Adapter.h"
#include "Builder/Builder.h"
#include "ObjectPools/ObjectPools.h"
#include "Filter/Filter.h"
#include "Flyweight/Flyweight.h"
#include "Bridge/Bridge.h"
#include "Command/Command.h"
#include "ChainOfResponsibility/ChainOfResponsibility.h"
#include "Decorator/Decorator.h"
#include "Observer/Observer.h"
#include "Observer/ObserverEx.h"
#include "FabricMethod/FabricMethod.h"
#include "Proxy/Proxy_Pattern_Tests.h"
#include "Singleton/Singleton.h"
#include "State/State.h"
#include "Mediator/Mediator.h"
#include "Memento/Memento.h"
#include "Strategy/Strategy.h"
#include "Visitor/Visitor.h"
#include "Visitor/StockVisitor.h"

int main(int argc, char** argv)
{
	Prototype::Test();

	// Interpreter::Test();

	// Adapter::TEST_ALL();
	// Adapter::USB_Adatpter::USB_Adapter_Test();

	// Flyweight::TestDriver();

	// Observer_Tests::Test1();
	// Observer_Tests_Ex::Test();

	// Singleton::TEST_ALL();;
	// DecoratorTests::Test();
	// FabricMethod::Test();

	// Proxy_Pattern_Tests::Test();
	// Proxy_Pattern_Tests::Test2();
	// Proxy_Pattern_Tests::Test3();

	// ChainOfResponsibility_Pattern_Tests::Test();
	// Command_Pattern_Tests::Test();
	// Composite::Test();

	// State_Pattern_Tests::Test();
	// Strategy_Pattern_Tests::Test();

	// AbstractFactory_Pattern_Tests::Test();
	// Builder_Pattern_Tests::Test();
	// Bridge::TEST_ALL();
	// ObjectPools::TEST_ALL();
	// Filter::Test();
	// Visitor_Pattern_Tests::test();
	// StockVisitor::test();

	// Memento::Driver();
	// Mediator::Test();

	std::this_thread::sleep_for(std::chrono::seconds(100));
}

